package main

import "fmt"
import "io/ioutil"
import b64 "encoding/base64"

func main() {
	buf, _ := ioutil.ReadFile("key.str")
	s := string(buf)
	sDec, _ := b64.StdEncoding.DecodeString(s)
	fmt.Println(string(sDec))
}
